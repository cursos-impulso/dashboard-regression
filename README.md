Este repositório contém um exemplo de um software para realizar a regressão do software de teste dashboard.

## Instalando

`bundle install`

## Configurando o ambiente

<!-- Cada ambiente possui um arquivo de configuração com as configurações específicas daquele ambiente para rodar os testes.
Os arquivos ficam na pastas `config`

Ambiente live: config/live.yml

A priori os ambientes já estão com as configurações corretas, mas caso seja necessária alguma alteração em algum ambiente é neste local que deve ser feita. -->

## Rodando os testes

`cucumber`

Para modificar o ambiente onde os testes rodaram basta definir a variável de ambiente ENV com o valor correspondente ao ambiente:

`ENV=live cucumber` roda no ambiente live

Por *padrão* se nenhum ambiente for passado os testes rodaram no *ambiente live*.

Caso deseja rodar os testes utilizando o navegador em modo headless basta utilizar o parâmetro `HEADLESS=true` na execução dos testes.
### Parâmetros para rodar os testes

Atualmente temos disponíveis os testes da applicação web (features/web) e os testes da aplicação web versão mobile (features/mobile)

Para rodar somente os testes de uma determinada feature:

`cucumber features/web/dasboard.feature`

Para rodar somente os testes com emails:

 `cucumber features/web/dasboard.feature --tags @smoke`

Não é incomum alguns testes falharem de forma aleatória por erros do selenium, então é sempre bom rodar o comando cucumber com o parâmetro do número de tentativas para que os casos de teste que falharam rodem novamente.

 `cucumber --retry 2`

Neste exemplo acima os casos de testes que falharem irão rodar até 2 vezes para tentarem passar.
Caso nenhum parâmetro seja passado a configuração padrão utilizada é __--retry 2__