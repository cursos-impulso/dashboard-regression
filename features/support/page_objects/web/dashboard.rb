# frozen_string_literal: true

module Web
  class Dashboard < SitePrism::Page
    set_url '/'

    element :first_number, '#calculator_widget_first_number'
    element :second_number, '#calculator_widget_second_number'
    element :calculate, '.btn.btn-primary'
    element :result, '#calculator-result'
  end
end
