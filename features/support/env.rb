# frozen_string_literal: true

require 'capybara'
require 'capybara/cucumber'
require 'report_builder'
require 'capybara/apparition'
require 'selenium-webdriver'
require 'rails-i18n'
require 'site_prism'
require 'site_prism/all_there'

I18n.load_path = $LOADED_FEATURES.select { |f| 'rails-i18n.rb'.in? f }.collect { |f| f.sub('lib/rails-i18n.rb', 'rails/locale/pt-BR.yml') }
I18n.locale = :'pt-BR'
I18n.default_locale = :'pt-BR'
