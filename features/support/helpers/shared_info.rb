# frozen_string_literal: true

require_relative 'methods_helpers'
require_relative 'replace_variables'

class SharedInfo
  include ::MethodHelpers
  include ::ReplaceVariables

  ENVIRONMENTS = %i[live].freeze
  TEST_SUIT = { mobile: 'mobile', web: 'web' }.freeze
  MAX_ATTEMPS = 25

  class << self
    attr_writer :config_params

    def config_params
      return @config_params unless @config_params.nil?

      @config_params = YAML.load_file(File.join(File.dirname(__FILE__), "../../../config/#{environment}.yml"))
    end

    def suit_name
      name = TEST_SUIT[:mobile] if mobile?
      name ||= TEST_SUIT[:web] if web?
      name
    end

    TEST_SUIT.values.each do |method|
      define_method "#{method}?" do
        instance_variable_get("@#{method}") == true
      end
    end

    def activate_test_suit(test_suit)
      raise "There is no test suit available named #{test_suit}" unless TEST_SUIT.values.include?(test_suit)

      @backoffice = @web = @api = @mobile = false
      instance_variable_set("@#{test_suit}", true)
      Capybara.app_host = host
    end

    def environment
      param_name = ENV['ENV'].to_s.downcase

      param_name = nil if param_name.empty?

      if !param_name.nil? && !ENVIRONMENTS.include?(param_name.to_sym)
        raise "There is no environment with name '#{param_name}'"
      end

      param_name ||= 'live'

      param_name
    end

    def debug_mode
      ENV['DEBUG_MODE'].to_s.downcase == 'true'
    end

    def host
      config_params['host'].gsub(%r{/$}, '')
    end
  end
end
