# frozen_string_literal: true

require 'active_support/time'
require 'faker'
module MethodHelpers
  attr_accessor :current_page_object

  def current_page_object=(value)
    @current_page_object = value
    Capybara.app_host = host
  end

  def page_object_mobule
    @page_object_mobule ||= suit_name.camelcase.constantize
    @page_object_mobule
  end

  def any_name
    Faker::Name.name
  end

  def any_phone
    Faker::PhoneNumber.cell_phone
  end

  def self.included(base)
    base.extend(MethodHelpers)
  end
end
