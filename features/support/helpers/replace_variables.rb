# frozen_string_literal: true

require 'active_support/time'

module ReplaceVariables
  def replace_variables(content)
    content_to_be_replaced = content.scan(/(:([a-zA-Z_\.]+):)/).map(&:first)

    if is_a_function?(content)
      function_name = content.gsub(':', '')
      return send(function_name)
    else
      value = variables_values(content_to_be_replaced)
    end
    value = value.is_a?(Array) ? value : [value]

    new_content = content.dup

    content_to_be_replaced.each_with_index do |replaced_content, index|
      new_content.gsub!(/#{replaced_content}/, value[index])
    end

    new_content.gsub!('"[', '[')
    new_content.gsub!(']"', ']')
    new_content.gsub!('"{', '{')
    new_content.gsub!('}"', '}')

    new_content
  end

  def is_a_function?(content)
    function_name = content.gsub(':', '')
    methods.include?(function_name.to_sym)
  end

  def variables_values(content_to_be_replaced)
    values = []

    content_to_be_replaced.map do |content|
      new_content = content.dup
      object_name, *methods = content.gsub(':', '').split('.')

      object = instance_variable_get("@#{object_name}")
      object ||= send(object_name)

      value = methods.empty? ? object : methods.map { |m| object = object.send(m) }.last

      parsed_value = value.is_a?(Hash) ? value.to_json : value.to_s

      new_content.gsub!(/#{content}/, parsed_value)
      values.push(new_content)
    end
    values
  end

  def self.included(base)
    base.extend(ReplaceVariables)
  end
end
