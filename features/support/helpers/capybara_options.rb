# frozen_string_literal: true

Before do |_scenario|
  args = %w[ignore-certificate-errors disable-popup-blocking no-sandbox disable-gpu --lang=pt-BR --start-maximized]
  args.push('--headless=true') if ENV['HEADLESS'] == 'true'

  chrome_options = {
    args: args,
    prefs: { 'intl.accept_languages': 'pt-BR' }
  }

  driver_name = :selenium_mobile if mobile?
  driver_name ||= :selenium

  if ENV['REMOTE']
    chrome_options.merge!({ mobileEmulation: { 'deviceName' => 'iPhone 6' } }) if mobile?
    capabilities = Selenium::WebDriver::Remote::Capabilities.chrome(chromeOptions: chrome_options)

    Capybara.register_driver driver_name do |app|
      Capybara::Selenium::Driver.new(
        app,
        browser: :remote,
        timeout: 30,
        desired_capabilities: capabilities,
        url: 'http://127.0.0.1:4444/wd/hub'
      )
    end

  else
    chrome_options.merge!({ emulation: { 'deviceName' => 'iPhone 6' } }) if mobile?
    options = Selenium::WebDriver::Chrome::Options.new(chrome_options)

    Capybara.register_driver driver_name do |app|
      Capybara::Selenium::Driver.new(app, browser: :chrome, timeout: 30, options: options)
    end
  end

  Capybara.configure do |config|
    config.default_driver = driver_name
    config.javascript_driver = driver_name
  end

  Capybara.page.driver.browser.manage.window.resize_to(1920, 1080) if ENV['HEADLESS'] == 'true'
end
