# frozen_string_literal: true

module CustomScreenshot
  def custom_take_screenshot(name_file, folder = 'reports/screenshots/test_screens')
    file_path = "#{folder}/#{name_file}.png"

    FileUtils.mkdir_p(folder) unless File.exist?(folder)

    save_screenshot(file_path, full: true)

    base64_img = Base64.encode64(File.open(file_path, 'r:UTF-8', &:read))
    attach(base64_img, 'image/png;base64')
  end
 end

World(CustomScreenshot)
