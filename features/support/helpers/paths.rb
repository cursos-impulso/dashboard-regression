# frozen_string_literal: true

module NavigationHelpers
  def page_object_of(page_name)
    page_path = {
      'Página Inicial' => page_object_mobule::Dashboard.new,
    }[page_name]

    if page_path.nil?
      raise "Não existe nenhum page object mapeado para a página '#{page_name}' no módulo '#{page_object_mobule}' arquivo 'paths.rb'"
    end

    page_path
  end
end

World(NavigationHelpers)
