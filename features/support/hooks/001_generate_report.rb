# frozen_string_literal: true

require 'report_builder'

at_exit do
  folders = %w[web mobile]

  folders.map do |folder_name|
    path = "reports/#{folder_name}"
    FileUtils.mkdir_p(path) unless File.exist?(path)
  end

  options = { input_path: {} }

  options[:input_path].merge!({ 'Web' => Dir['reports/web/*.json'] }) if Dir['reports/web/*.json'].any?
  options[:input_path].merge!({ 'Mobile' => Dir['reports/mobile/*.json'] }) if Dir['reports/mobile/*.json'].any?

  options.merge!({
                   report_path: 'reports/pretty_report',
                   report_types: ['html'],
                   report_title: "Relatório de Execução dos Testes no Ambiente #{environment.camelcase}"
                 })

  ReportBuilder.build_report options
end
