# frozen_string_literal: true

Before do |scenario|
  all_tags = scenario.tags.map(&:name).join(',')

  skip_this_scenario if all_tags =~ /skip/
end
