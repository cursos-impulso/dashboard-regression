# frozen_string_literal: true

Before do |scenario|
  location = scenario.location.to_s
  test_suit = location.match(/mobile|web/).to_s.to_sym
  SharedInfo.activate_test_suit(SharedInfo::TEST_SUIT[test_suit])
end
