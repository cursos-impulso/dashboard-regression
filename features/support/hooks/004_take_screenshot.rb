# frozen_string_literal: true

After do |scenario|
  name = scenario.name.gsub(%r{([_@#!%()\-=;><,{}\~\[\]\./\?\"\*\^\$\+\-]+)}, '')

  from = 'àáäâãèéëẽêìíïîĩòóöôõùúüûũñç'
  to = 'aaaaaeeeeeiiiiiooooouuuuunc'
  name = name.gsub(/[#{from}]/, from.split('').zip(to.split('')).to_h)

  name = suit_name + '_' + name.split(' ').join('_')

  screenshot_path = (custom_take_screenshot(name, 'reports/screenshots/test_failed') if scenario.failed?)
end
