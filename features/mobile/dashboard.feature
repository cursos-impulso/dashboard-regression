#language: pt

Funcionalidade: Vizualizar dashboard
    COMO um cliente EU QUERO visualizar o dashboard

        @smoke
        Cenário: Realizar soma de número no widget de calculadora
            Dado Que eu inicio na "Página Inicial"
              E Eu preencho o seguinte:
                  | first_number  | 10 |
                  | second_number | 30 |
             Quando Eu pressiono o botão "calculate"
              E Eu espero "1" segundo
              E Eu verifico que "result" é igual a "40.0"
