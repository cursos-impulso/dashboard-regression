#language: pt

Funcionalidade: Vizualizar dashboard
    COMO um cliente EU QUERO visualizar o dashboard

        @smoke
        Cenário: Realizar soma de números positivos no widget de calculadora
            Dado Que eu inicio na "Página Inicial"
              E Eu preencho o seguinte:
                  | first_number  | 10 |
                  | second_number | 30 |
             Quando Eu pressiono o botão "calculate"
              E Eu espero "1" segundo
              E Eu verifico que "result" é igual a "40.0"

        Cenário: Realizar soma de números negativos no widget de calculadora
            Dado Que eu inicio na "Página Inicial"
              E Eu preencho o seguinte:
                  | first_number  | -10 |
                  | second_number | -20 |
             Quando Eu pressiono o botão "calculate"
              E Eu espero "1" segundo
              E Eu verifico que "result" é igual a "-30.0"

        @smoke
        Cenário: Realizar soma de números negativos e positivos no widget de calculadora
            Dado Que eu inicio na "Página Inicial"
              E Eu preencho o seguinte:
                  | first_number  | -10 |
                  | second_number | 60  |
             Quando Eu pressiono o botão "calculate"
              E Eu espero "1" segundo
              E Eu verifico que "result" é igual a "50.0"