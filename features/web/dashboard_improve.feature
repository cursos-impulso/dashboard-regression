#language: pt

Funcionalidade: Vizualizar dashboard
    COMO um cliente EU QUERO visualizar o dashboard

        Esquema do Cenário: Realizar widget calculadora
            Dado Que eu inicio na "Página Inicial"
              E Eu preencho o seguinte:
                  | first_number  | <first_number>  |
                  | second_number | <second_number> |
             Quando Eu pressiono o botão "calculate"
              E Eu espero "1" segundo
              E Eu verifico que "result" é igual a "<total>"
        Exemplos:
                  | first_number | second_number | total |
                  | 10           | 30            | 40.0  |
                  | -10          | -20           | -30.0 |
                  | -10          | 60            | 50.0  |

