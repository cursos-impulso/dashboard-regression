# frozen_string_literal: true

require 'capybara'
require 'capybara/dsl'

When(/^(?:|Que eu|Eu) (?:|inicio na|vou para) "([^"]*)"$/) do |page_name|
  self.current_page_object = page_object_of(page_name)

  current_page_object.load
  page_was_changed
end

Then(/^(?:|Que eu|Eu) (?:estou|estarei) na "([^"]*)"$/) do |page_name|
  expected_page = page_object_of(page_name)

  expect(expected_page).to be_displayed

  self.current_page_object = expected_page
  page_was_changed
end

def page_was_changed
  @old_url = current_page_object.current_url
end

When(/^(?:|Que eu|Eu) (?:|sigo|clico em|clico no|clico na) "([^"]*)"$/) do |link_name|
  link = current_page_object.link_of(link_name)

  link.click
  page_was_changed
end

When(/^(?:|Eu) preencho o seguinte:$/) do |fields|
  fields.rows_hash.each do |name, value|
    Kernel.puts "Preenchendo '#{name}' com '#{value}'" if debug_mode
    step %(Eu preencho "#{name}" com "#{value}")
  end
end

When(/^(?:|Que eu|Eu) preencho "([^"]*)" com "([^"]*)"$/) do |field, value|
  field.gsub!('-', '_')
  custom_value = replace_variables(value) if value.match(/:.*:/)
  custom_value ||= value

  current_page_object.send(field).set custom_value
end

When(/^(?:|Que eu|Eu) pressiono o botão "([^"]*)"$/) do |button_name|
  current_page_object.send(button_name).click
end


When(/^(?:|Eu) verifico que "([^"]*)" é igual a "([^"]*)"$/) do |element, expected_value|
  expect(current_page_object.send(element).text).to eq(expected_value.to_s)
end

When(/^(?:|Eu) espero a próxima página carregar$/) do
  attemps = 0

  base_url = current_url.split('?').first
  base_old_url = @old_url.split('?').first

  while base_url == base_old_url
    attemps += 1
    Kernel.puts "Esperando mudar da página antiga '#{@old_url}'. A página atual é '#{current_url}' (tentativa #{attemps} de #{max_attemps})"
    sleep(1)
    break if attemps >= max_attemps

    base_url = current_url.split('?').first
  end

  Kernel.puts "Mudou da página antiga '#{@old_url}' para a página atual '#{current_url}'"

  expect(current_url.dup).to_not be(@old_url)
end

When(/^(?:|Eu) espero a tela de loading desaparecer$/) do
  Kernel.puts "Esperando o loading desaparecer no page_object '#{current_page_object.class.name}'" if debug_mode
  current_page_object.wait_until_loading_invisible(wait: 30)
end

When(/^Eu espero "(\d+)" segundos?$/) do |seconds|
  sleep seconds.to_i
end

Then(/^(?:|Eu) verifico que o texto "([^"]*)" está presente em "([^"]*)"$/) do |info, context|
  context.gsub!('-', '_')

  expect(current_page_object.send(context).text).to have_text(info)
end

Given(/^(?:|Que eu) preencho o formulário de esqueci a senha$/) do
  step %(Que eu vou para "Página de Login")
  step %(Eu clico em "Esqueci minha senha")
  step %(Eu estou na "Página Esqueci Minha Senha")
  step %(Eu preencho "email" com ":user.email:")
  step %(Eu pressiono o botão "Redefinir senha")
end

def method_missing(method, *args, &block)
  if SharedInfo.methods.include?(method.to_sym)
    SharedInfo.send(method, *args)
  else
    super
  end
end
